local fs = neo.requireAccess("x.neo.pub.base", "fs")
local fl = fs.open("/cfg.lua", false)
local cfg = fl.read("*a")
fl.close()
cfg = require("serial").deserialize(cfg)
local remaining = {}
for k, _ in pairs(cfg.mapping) do
 remaining[k] = true
end
local t = neo.requireAccess("c.tunnel", "tun").list()()
neo.requireAccess("s.h.modem_message", "listen")
t.send("KDG;;PING")
neo.scheduleTimer(os.uptime() + 10)
local totalA = ""
local totalB = ""
while true do
 local a, b, c, d, e, f, g = coroutine.yield()
 if a == "h.modem_message" and f == "KDG;PING" then
  g = g:sub(8):gsub("%.0", "")
  if not remaining[g] then
   totalB = totalB .. g .. "\n"
  else
   remaining[g] = nil
  end
 elseif a == "k.timer" then
  break
 end
end
for k, v in pairs(remaining) do
 totalA = totalA .. k .. "\n"
end
local total = totalA .. "Unknown:\n" .. totalB
local event = require("event")(neo)
local neoux = require("neoux")(event, neo)
neoux.startDialog(total, "Issues At", true)
