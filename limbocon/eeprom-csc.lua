-- Telescreen Colour Edition (Sizing)
local e = "KICS;" .. component.proxy(component.list("eeprom", true)()).getLabel()
local scr = component.list("screen", true)()
local gpu = component.proxy(component.list("gpu", true)())
gpu.bind(scr)
gpu.setResolution(1, 1)
local resW, resH = 1, 1
while true do
 local ev = {computer.pullSignal()}
 if ev[1] == "modem_message" then
  if ev[6] == e then
   if ev[12] ~= resW or ev[13] ~= resH then
    resW, resH = ev[12], ev[13]
    gpu.setResolution(resW, resH)
   end
   gpu.setBackground(ev[7])
   gpu.setForeground(ev[8])
   gpu.set(ev[9], ev[10], ev[11])
  end
 end
end