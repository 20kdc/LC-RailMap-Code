-- Central Control Computer --
-- This is released into the public domain.
-- No warranty is provided, implied or otherwise.

-- vJun14;16:41
local ic = neo.requireAccess("x.neo.pub.base", "icecap")
neo.requireAccess("s.h.modem_message", "kdg recv.")
local tun = neo.requireAccess("c.tunnel", "displayboard send").list()()
local cfg = ic.open("/cfg.lua", false)
local s = require("serial").deserialize(cfg.read("*a"))
cfg.close()

local mapping = s.mapping
local iTypes = s.iTypes

local function execTransition(iType, tTypes)
 local img = ic.open("/" .. iType .. ".lua", false)
 -- Serialized by the same library on 20kdc's comp.'s Lua install
 -- Probably Compatible (tm)
 local im = require("serial").deserialize(img.read("*a"))
 img.close()
 for k, _ in pairs(tTypes) do
  for _, v in ipairs(im[k]) do
   local y = math.floor((v[1] - 1) / im.sW) + 1
   local x = v[1] - ((y - 1) * im.sW)
   local str = v[2]
   local a, b = tun.send("KICS;" .. iType, v[3], v[4], x, y, str, im.sW, im.sH)
   assert(a, "PKT fail " .. tostring(b))
  end
 end
end
for k, _ in pairs(iTypes) do
 execTransition(k, {["init"] = true})
end
-- Maps this: (BTYPE ";" PID) to their on/off ("+"/"-")
local states = {}
while true do
 local ev = {coroutine.yield()}
 if ev[1] == "h.modem_message" then
  local dt = tostring(ev[6])
  if dt == "I absolutely want to reset the CCC" then
   for k, v in pairs(iTypes) do
    local tMap = {["init"] = true}
    for bt, st in pairs(states) do
     if st == "+" then
      if bt:sub(1, #v + 1) == v .. ";" then
       tMap["+" .. bt:sub(#v + 2)] = true
      end
     end
    end
    execTransition(k, tMap)
   end
  end
  if dt:sub(1, 4) == "KDG;" then
   local nt = dt:sub(5):gsub("%.0", "")
   local ex = mapping[nt]
   if ex then
    local mods = {}
    local nStates = {}
    for _, v in ipairs(ex) do
     local base = v:sub(2):match("^[^;]+")
     local pid = v:sub(2 + #base + 1)
     mods[base] = mods[base] or {}
     if states[base .. ";" .. pid] ~= v:sub(1, 1) then
      nStates[base .. ";" .. pid] = v:sub(1, 1)
      mods[base][v:sub(1, 1) .. pid] = true
     end
    end
    for k, v in pairs(nStates) do
     states[k] = v
    end
    for k, v in pairs(iTypes) do
     if mods[v] then
      execTransition(k, mods[v])
     end
    end
   end
  end
 end
end