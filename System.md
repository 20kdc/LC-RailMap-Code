# The System

There are 3 kinds of computers in the system used here,
 and all of them talk via Creative Linked Cards, which share a network.

## eeprom-kdg.lua

This thing's job is to move train overhead events to the network.

The only further thing it does is make itself more convenient to install by
 reporting the coordinates of detectors connected to it.

Inside the computer, it only needs the bare essentials and a Creative Linked Card.

You should use Lua 5.3 mode for consistency.

Connect up rail detectors via adapters, add a chat box,
 boot it up, remove chat box.

(The chat box is optional, but recommended.)

Note: If you see ".0" in these, this is removed by app-ccc before it's
 actually used.

## eeprom-csc.lua

Colour resolution-changable remote screen set to a "stream" based on the EEPROM name.

The CCC emits these streams.

## app-ccc.lua

KittenOS NEO "application". Should really be a service,
 but I never got around to it, and having it in the launcher is quicker to access.

Just don't run two at once. I forgot to put any safeguard in against that.

Anyway, this gets the magic data from `/data/app-ccc/cfg.lua` and friends in the same directory.

Specifically, you'll want cfg.lua, and *everything in the "output" directory of this repository*, there.

(Apart from mechabunny. You can avoid copying that.)

For the actual format of files involved, going to just recommend reading the code.

I'm tired and I want to make sure this is releasable by the time I get up tomorrow.

