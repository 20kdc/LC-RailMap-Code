-- This is released into the public domain.
-- No warranty is provided, implied or otherwise.

local bmF, txF = ...
-- Get from OC-KittenOS
local bmp = require("bmp")
local serial = require("serial")

unicode = utf8

local braille = require("braille")
local f = io.open(bmF, "rb")
local img = f:read("*a")
f:close()
local f = io.open(txF, "rb")
local itx = serial.deserialize(f:read("*a"))
f:close()
local m = bmp.connect(function (x) return img:byte(x) end)

local sWidth, sHeight = math.floor(m.width / 2), math.floor(m.height / 4)

local function genBuffer(visCols)
 -- each pixel is {ch, bg, fg}
 local pixels = {}
 for i = 1, sWidth * sHeight do
  pixels[i] = {" ", 0, 0xFFFFFF}
 end
 local function span(x, y, str, bg, fg)
  local idx = x + ((y - 1) * sWidth)
  for ipx = 0, utf8.len(str) - 1 do
   local cho = utf8.offset(str, 2)
   local px = str:sub(1, cho - 1)
   str = str:sub(cho)
   pixels[idx + ipx] = {px, bg, fg}
  end
 end
 for y = 1, sHeight do
  braille.calcLine(1, y, sWidth, span, function (xo, yo)
   yo = yo + ((y - 1) * 4)
   local px = m.getPixel(xo, yo, 0)
   if visCols[px] then
    if ((xo + yo) % 2) == 0 then
     px = 0
    end
   end
   local pm = m.getPalette(px)
   return (pm >> 16) & 0xFF, (pm >> 8) & 0xFF, pm & 0xFF
  end, 0)
 end
 local function tx(x, y, text)
  x = math.floor(x / 2) + 1
  y = math.floor(y / 4) + 1
  span(x, y, text, 0xFFFFFF, 0)
 end
 for _, v in ipairs(itx) do
  tx(v[1], v[2], v[3])
 end
 return pixels
end
local function genDiff(a, b)
 local areas = {}
 local areaStart, areaCode, areaBG, areaFG
 local function flushArea()
  if areaStart then
   table.insert(areas, {areaStart, areaCode, areaBG, areaFG})
   areaStart, areaCode, areaBG, areaFG = nil
  end
 end
 for i = 1, sWidth * sHeight do
  if (i - 1) % sWidth == 0 then
   flushArea()
  end
  if a and (a[i][1] == b[i][1] and a[i][2] == b[i][2] and a[i][3] == b[i][3]) then
   flushArea()
  else
   if areaBG ~= b[i][2] or areaFG ~= b[i][3] then
    flushArea()
    areaStart = i
    areaCode = ""
    areaBG, areaFG = b[i][2], b[i][3]
   end
   areaCode = areaCode .. b[i][1]
  end
 end
 flushArea()
 return areas
end

local fTable = {}

fTable["sW"] = sWidth
fTable["sH"] = sHeight

local baseBuffer = genBuffer({})
fTable["init"] = genDiff(nil, baseBuffer)
for i = 2, m.paletteCol do
 local exBuffer = genBuffer({[i] = true})
 fTable["+" .. i] = genDiff(baseBuffer, exBuffer)
 fTable["-" .. i] = genDiff(exBuffer, baseBuffer)
end

io.write(serial.serialize(fTable))
