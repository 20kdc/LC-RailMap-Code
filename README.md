# Documentation and Code for LimboCon Rail Map System

This documentation and code is published mostly
 because cam72cam noted about people in the IR discord possibly
 "going nuts over it".

I figured I should make it available from somewhere.

## License

This is released into the public domain.
No warranty is provided, implied or otherwise.

## Credits

20kdc: All files in this repository, installing detectors

Skye: The general shape of the early versions of EALL.bmp,
 a lot of design advice, colour scheme for the final EALL.bmp

Ristelle, bballboy2002, cam72cam:
 Fixing the detectors and screens that were going wrong
 by adding more chunkloaders, installing the screens,
 generally a lot of stuff where the station or rail system design
 itself was involved, and of course fixing those times when I kept breaking rails.
 You're allowed to slap me, 20kdc, in the face if you ever meet me.
 (That said, this is valid a total of once per person.)

## Known Flaws

### Repository is a mess

Making this into an easy-to-use package was not exactly a priority,
 given that I'm not totally sure there won't be further changes up ahead.

### CCC requires KittenOS NEO

Porting the CCC to not need KittenOS NEO, or indeed any OS,
 should be relatively easy.

It's not a particularly complicated program.

Serialization differences can be dealt with by copying serial.lua from 
 KittenOS NEO and putting it into the lib directory of OpenOS.

Or just changing around the format used by stuff.

### Screen desync due to chunk unloading

It is possible that if a screen receiver computer were to be unloaded,
 it would miss events, leading to what would normally appear to be a 
 missed detector. This hasn't turned out to be an issue, because all
 the screen receivers need to be chunkloaded anyway to prevent them dying
 from out of game issues beyond our control.

If it does become an issue, modify app-ccc to send out less differential
 updates. Note, however, that this may cause the CCC to become even more
 laggy than before, including causing more lag on clients near the maps.
